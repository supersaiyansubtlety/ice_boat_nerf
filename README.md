<!--required for Modrinth centering -->
<center>
<div style="text-align:center;line-height:250%">

## Ice Boat Nerf

[![Minecraft versions](https://cf.way2muchnoise.eu/versions/Minecraft_394834_all.svg)](https://modrinth.com/mod/ice-boat-nerf/versions#all-versions)
![environment: both](https://img.shields.io/badge/environment-both-4caf50)  
[![loader: Fabric](https://img.shields.io/badge/loader-Fabric-cdc4ae?logo=data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgMjYgMjgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgc3R5bGU9ImZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDoyIj48cGF0aCBkPSJtMTAgMjQtOC04TDE0IDRWMmgybDYgNnY0TDEwIDI0eiIgc3R5bGU9ImZpbGw6I2RiZDBiNCIvPjxwYXRoIGQ9Ik0xMiA2djJoLTJ2Mkg4djJINnYySDR2MkgydjRoMnYyaDJ2MmgydjJoMnYtMmgydi0yaDJ2LTJoMnYtMmgydi0yaDJ2LTRoLTJ2LTJoLTJWOGgtMlY2aDJ2MmgydjJoMnYyaDJ2Mmgydi0yaC0yVjhoLTJWNmgtMlY0aC0yVjJoMnYyaDJ2MmgydjJoMnYyaDJ2NGgtMnYyaC00djJoLTJ2MmgtMnYyaC0ydjRoLTJ2Mkg4di0ySDZ2LTJINHYtMkgydi0ySDB2LTRoMnYtMmgydi0yaDJ2LTJoMlY4aDJWNmgyVjJoMlYwaDJ2MmgtMnY0aC0yIiBzdHlsZT0iZmlsbDojMzgzNDJhIi8+PHBhdGggc3R5bGU9ImZpbGw6IzgwN2E2ZCIgZD0iTTIyIDEyaDJ2MmgtMnoiLz48cGF0aCBkPSJNMiAxOGgydjJoMnYyaDJ2MmgydjJIOHYtMkg2di0ySDR2LTJIMnYtMiIgc3R5bGU9ImZpbGw6IzlhOTI3ZSIvPjxwYXRoIGQ9Ik0yIDE2aDJ2MmgydjJoMnYyaDJ2Mkg4di0ySDZ2LTJINHYtMkgydi0yeiIgc3R5bGU9ImZpbGw6I2FlYTY5NCIvPjxwYXRoIGQ9Ik0yMiAxMnYtMmgtMlY4aC0yVjZsLTIuMDIzLjAyM0wxNiA4aDJ2MmgydjJoMnpNMTAgMjR2LTJoMnYtNGg0di00aDJ2LTJoMnY0aC0ydjJoLTJ2MmgtMnYyaC0ydjJoLTIiIHN0eWxlPSJmaWxsOiNiY2IyOWMiLz48cGF0aCBkPSJNMTQgMThoLTR2LTJIOHYtMmgydjJoNHYyem00LTRoLTR2LTJoLTJ2LTJoLTJWOGgydjJoMnYyaDR2MnpNMTQgNGgydjJoLTJWNHoiIHN0eWxlPSJmaWxsOiNjNmJjYTUiLz48L3N2Zz4=)](https://fabricmc.net/)
<a href="https://quiltmc.org/"><img alt="available for: Quilt Loader" width=73 src="https://raw.githubusercontent.com/QuiltMC/art/master/brand/1024png/quilt_available_dark.png"></a>  
<a href="https://modrinth.com/mod/fabric-api/versions"><img alt="Requires: Fabric API" width="60" src="https://i.imgur.com/Ol1Tcf8.png"></a>
[![supports: Mod Menu](https://img.shields.io/badge/supports-Mod_Menu-134bfe?logo=data:image/webp+xml;base64,UklGRlIAAABXRUJQVlA4TEUAAAAv/8F/AA9wpv9T/M///McDFLeNpKT/pg8WDv6jiej/BAz7v+bbAKDn9D9l4Es/T/9TBr708/Q/ZeBLP0//c7Xiqp7z/QEA)](https://modrinth.com/mod/modmenu/versions)  
[![license: MIT](https://img.shields.io/badge/license-MIT-white)](https://gitlab.com/supersaiyansubtlety/ice_boat_nerf/-/blob/master/LICENSE)
[![source on: GitLab](https://img.shields.io/badge/source_on-GitLab-fc6e26?logo=gitlab)](https://gitlab.com/supersaiyansubtlety/ice_boat_nerf)
[![issues: GitLab](https://img.shields.io/gitlab/issues/open-raw/supersaiyansubtlety/ice_boat_nerf?label=issues&logo=gitlab)](https://gitlab.com/supersaiyansubtlety/ice_boat_nerf/-/issues)
[![localized: Percentage](https://badges.crowdin.net/ice-boat-nerf/localized.svg)](https://crwd.in/ice-boat-nerf)  
[![Modrinth: Downloads](https://img.shields.io/modrinth/dt/ice-boat-nerf?logo=modrinth&label=Modrinth&color=00ae5d)](https://modrinth.com/mod/ice-boat-nerf/versions)
[![CurseForge: Downloads](https://img.shields.io/curseforge/dt/394834?logo=curseforge&label=CurseForge&color=f16437)](https://www.curseforge.com/minecraft/mc-mods/ice-boat-nerf/files)  
[![chat: Discord](https://img.shields.io/discord/1006391289006280746?logo=discord&color=5964f3)](https://discord.gg/xABmPngXAH)
<a href="https://coindrop.to/supersaiyansubtlety"><img alt="coindrop.to me" width="82" style="border-radius:3px" src="https://coindrop.to/embed-button.png"></a>
<a href="https://ko-fi.com/supersaiyansubtlety"><img alt="Buy me a coffee" width="106" src="https://i.ibb.co/4gwRR8L/p.png"></a>

</div>
</center>

---

### Makes it so boats don't go extra fast on ice.

Must be installed on both client and server.

There are also two(!) Forge ports:
- [Boat Behavior Tweaks](https://www.curseforge.com/minecraft/mc-mods/boat-behavior-tweaks) by
[chibl](https://www.curseforge.com/members/chibl/projects), for 1.16 versions
- [Ice Boat Nerf - Reforged](https://www.curseforge.com/minecraft/mc-mods/ice-boat-nerf-reforged) by
[kawaiicakes](https://www.curseforge.com/members/kawaiicakes/projects), for newer versions

This mod makes it so boats don't go any faster on ice blocks than they do on dirt.

Why?

Well it doesn't make a lot of sense that in vanilla boats move faster on ice than on water.

Also, there are loads of more interesting ways of traveling quickly in minecraft that are largely made obsolete by the existence of ice boats, like:
- piston bolts
- dolphin highways
- simple railways
- piston worms

<img alt="chicken watching boat" width=500 src="https://lh3.googleusercontent.com/nQgj_OzYuXMMYVMk9Gw5Houl72MwgV1Ek2KfLM3Pp0VeCkegV0u1OeYa3xI0z-3tq96TAp6lJHdo9izcnB63VlftsXSlZcbgTzuDDJFRlwFbqWBQ_fTv2eB1MhkeJ7xuQ7z5eTdamflvBdPOr4AxHoUArVQn0Dq8ZUHDUd-OgrtOwgi4ZXtKNa5FZ8yAvg_J3sAZGozVdAQD_FmbQLEG8uysQRilwSvjaBnB608pj8RtjIb-53z3Qcp5mWH9GUIukUXbe4kN0zMUebwy4PyS1lt3QFApAXYc6lS1gfZnp0SqLAmMgl5zv7SwSLRJ7OXKhRJD1Bm8EJcJIzHdCpxMeDUA-MlcZgNkhZfO_i34svUCeh36_tpY4OmZsFo8V5KxjjQyEhBWHni-rnws9m0NOR3gF0vT1IQ8L1CsY5XnBkVF786As7TYgw9NtsFTBvcutFiIG7i-qOE8yR_D6WGZsR8vXIvfRuz4lQj6sNI8XlXXK9NkRJzmmNw7XZ-uBhItwNrwof_6S5w_v40bFy6xBUUbxAaC-c11oxFOZ7W1xy6QYBvhrOGTNyn5CsxFVFNFgI8nuOXPYmF4YrShuMlNLBrLa-KWfzI8gobok51uEfWIFkIQNYP_cGQjtrt9mlpN9nFPgVsvdAXSNHewQKSs4FaSdLa3wvUUbXUV3R_OPW_e7DfMErES3GSjlRVC_jKukqMb=w3845-h1880-ft">

So if you want to make use of all these other interesting mechanics but don't want to feel like your effort is wasted
on these complex but second-rate forms of transport, install this mod!

Enjoy the realism of chickens casually walking past your boat as you try in vain to propel it forward by scraping the
paddles across the surface of the ice!

---

<details>

<summary>Translating</summary>

[![localized: Percentage](https://badges.crowdin.net/ice-boat-nerf/localized.svg)](https://crwd.in/ice-boat-nerf)  
If you'd like to help translate Ice Boat Nerf, you can do so on
[Crowdin](https://crwd.in/ice-boat-nerf).

</details>

---

This mod is only for Fabric (works on Quilt, too!) and I won't be porting it to Forge. The license is MIT, however,
~~so anyone else is free to port it~~ and [chibl](https://www.curseforge.com/members/chibl/projects) and
[kawaiicakes](https://www.curseforge.com/members/kawaiicakes/projects) have each ported it to Forge!

I'd appreciate links back to this page if you port or otherwise modify this project, but links aren't required.
