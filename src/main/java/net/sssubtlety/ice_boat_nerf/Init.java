package net.sssubtlety.ice_boat_nerf;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.networking.v1.ServerLoginConnectionEvents;
import net.fabricmc.fabric.api.networking.v1.ServerLoginNetworking;
import net.minecraft.text.Text;

import static net.sssubtlety.ice_boat_nerf.IceBoatNerf.VERIFICATION_CHANNEL;

public class Init implements ModInitializer {
    @Override
    public void onInitialize() {
        ServerLoginConnectionEvents.QUERY_START.register((handler, server, sender, synchronizer) ->
            sender.sendPacket(VERIFICATION_CHANNEL, PacketByteBufs.empty())
        );

        ServerLoginNetworking.registerGlobalReceiver(
            VERIFICATION_CHANNEL,
            (server, handler, understood, buf, synchronizer, responseSender) -> {
                if (!understood) {
                    handler.disconnect(Text.literal("Client is missing Ice Boat Nerf (1.0.17 or newer)"));
                }
            }
        );
    }
}
