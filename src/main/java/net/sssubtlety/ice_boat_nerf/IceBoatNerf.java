package net.sssubtlety.ice_boat_nerf;

import net.minecraft.util.Identifier;

public class IceBoatNerf {
    public static final String NAMESPACE = "ice_boat_nerf";
    public static final Identifier VERIFICATION_CHANNEL = Identifier.of(NAMESPACE, "verification");
}
