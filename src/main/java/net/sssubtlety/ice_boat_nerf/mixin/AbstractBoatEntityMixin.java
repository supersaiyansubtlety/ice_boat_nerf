package net.sssubtlety.ice_boat_nerf.mixin;

import com.llamalad7.mixinextras.injector.ModifyReturnValue;

import net.minecraft.entity.vehicle.AbstractBoatEntity;
import net.minecraft.util.math.MathHelper;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;

@Mixin(AbstractBoatEntity.class)
public class AbstractBoatEntityMixin {
	@ModifyReturnValue(method = "getLandSlipperiness", at = @At("RETURN"))
	private float clampSlipperiness(float original) {
		return (float) MathHelper.clamp(original, 0, 0.45);
	}
}
