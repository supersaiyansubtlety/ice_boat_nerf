package net.sssubtlety.ice_boat_nerf;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.networking.v1.ClientLoginConnectionEvents;
import net.fabricmc.fabric.api.client.networking.v1.ClientLoginNetworking;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayConnectionEvents;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.minecraft.network.DisconnectionDetails;
import net.minecraft.text.Text;

import java.util.concurrent.CompletableFuture;

import static net.sssubtlety.ice_boat_nerf.IceBoatNerf.VERIFICATION_CHANNEL;

public class ClientInit implements ClientModInitializer {
    private static final DisconnectionDetails SERVER_MISSING_MOD = new DisconnectionDetails(Text.translatable(
        "multiplayer.ice_boat_nerf.disconnect.server_missing_mod"
    ));

    private static boolean validServer;

    @Override
    public void onInitializeClient() {
        ClientLoginConnectionEvents.QUERY_START.register((handler, client) -> validServer = false);

        ClientLoginNetworking.registerGlobalReceiver(
            VERIFICATION_CHANNEL,
            (client, handler, buf, listenerAdder) -> {
                validServer = true;
                return CompletableFuture.completedFuture(PacketByteBufs.empty());
            }
        );

        ClientPlayConnectionEvents.JOIN.register((handler, sender, client) -> {
            if (!validServer) {
                handler.onDisconnected(SERVER_MISSING_MOD);
            }
        });
    }
}
